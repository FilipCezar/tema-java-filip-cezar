package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import veterinarycabinet.Animal;
import veterinarycabinet.Personalmedical;
import veterinarycabinet.Programare;

public class DatabaseUtil { // se ocupa de conexiune
	public static EntityManagerFactory entityManagerFactory; // se ocupa de tranzactii
	public static EntityManager entityManager;
	
	public void setUp()  { // apar exceptii in cazul nu se poate conecta la baza de date
		entityManagerFactory = Persistence.createEntityManagerFactory("cveterinarycabinet");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void saveAnimal(Animal animal) { // functia de salvare
		entityManager.persist(animal);
	}
	
	public void savePersonalmedical(Personalmedical personalmedical) {
		entityManager.persist(personalmedical);
	}
	
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() { // functie care se opreasca conexiunea
		entityManager.close();
	}
	
	public List<Animal> animalList(){
		List<Animal> animalList = (List<Animal>) entityManager.createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
		return animalList;
	}
	
	
	
	
	/*public void printAllAnimalsFromDB() {
		List<Animal> schedules = entityManager.createNativeQuery("Select * from veterinarycabinet.Animal",Animal.class).
				getResultList();
			System.out.println("Animal :" + animal.getName() + "has ID" + animal.getIdAnimal());
		}
	
	}*/
	
	public void printAllMeetingsFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from cabinetveterinar.programare Order by data",
				Programare.class).getResultList();
		for(Programare programare : results) {
			//identifies the unique entity which has a given primary key in the specified table, then gets its name
			System.out.println(entityManager.find(Animal.class, programare.getIdAnimal()).getName() + 
					" este programat in data " + programare.getData() +
					" la ora " + programare.getOra() + 
					" la doctorul " + entityManager.find(Personalmedical.class,  programare.getIdPersonalMedical()).getNume());
		}
	}

}
