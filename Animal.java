package veterinarycabinet;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	@Temporal(TemporalType.DATE)
	private Date birthDate;

	private String breed;

	private int idProgramare;

	private String name;

	private int ownerName;

	private String typeProgramare;

	private int weight;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBreed() {
		return this.breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOwnerName() {
		return this.ownerName;
	}

	public void setOwnerName(int ownerName) {
		this.ownerName = ownerName;
	}

	public String getTypeProgramare() {
		return this.typeProgramare;
	}

	public void setTypeProgramare(String typeProgramare) {
		this.typeProgramare = typeProgramare;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}