package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/*import java.sql.Time;
import java.sql.Date;

import veterinarycabinet.Animal;
import veterinarycabinet.Personalmedical;
import veterinarycabinet.Programare;
import util.DatabaseUtil;

public class main {

	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp(); // initialises the entities
		dbUtil.startTransaction();
		//dbUtil.saveAnimal/Programare/Personalmedical // saves smth into some table
		dbUtil.commitTransaction(); // commits any changes made
		dbUtil.printAllMeetingsFromDB();
		dbUtil.closeEntityManager(); // closes the entity
		
		
	}
*/

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		}
	public static void main(String[] args) {
		launch(args);
	}
	
}
